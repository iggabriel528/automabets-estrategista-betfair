const TelegramChat = require("./app/models/chat");
const Monitor = require("./app/models/monitor");
const RouletteParam = require("./app/models/rouletteparam");
const Adm = require("./app/models/adm");
const bcryptjs = require("bcryptjs");
const Socket = require("./app/models/socket");
const Message = require("./app/models/message");

(async()=>{
    var newadm = false;
    var adm = { _id: undefined };
    if(newadm){
        adm = await Adm.create({name:'Tanos',email:'contato@atlantissoftware.com.br',password:bcryptjs.hashSync('f$Lfktw#!hsl',10)});
    }else{
        adm._id = '622250b09bea1336ca0d3f4f';
    }
    const monitor = await Monitor.create({user_id:adm._id,name:'4 - Roleta - Playtech - Betfair',active:false,game:'betfair_playtech'});
    if(monitor){
        const roulettes = [
            'Turbo Roulette', 'Who Wants To Be a Millionaire? Roulette', 'Mega Fire Blaze Roulette Live',
            'Football Roulette', 'Quantum Roulette Live', 'Spin & Win Roulette', 'Roulette', 'American Roulette', 
            'Spread Bet Roulette', 'Deutsches Roulette', 'Hindi Roulette', 'Quantum Auto Roulette', 'Prestige Roulette', 
            'Speed Roulette', 'Slingshot', 'Triumph Roulette', 'Quantum Roulette Italiana', 'Roulette Italiana', 
            'Roleta Brasileira', 'Turkish Roulette', 'Greek Roulette', 'Betfair Roulette']
        
        const strategies = [
            {strategy:'repetition',substrategies:['color','oddev','hilow','column','dozen']},
            {strategy:'absense',substrategies:['column','dozen']},
            {strategy:'alternation',substrategies:['color','oddev','hilow']}
        ]

        for(const roulette of roulettes){
            for(const strategy of strategies){
                for(const substrategy of strategy.substrategies){
                    await RouletteParam.create({
                        monitor_id:monitor._id,
                        roulette,
                        strategy:strategy.strategy,
                        substrategy:substrategy,
                        param:12,
                        cover_zero:true,
                        active:false,
                        attempts:3
                    })   
                }
            }
        }
        const messages = [
            {category:'warning',lines:[
                {line:'title',present:true,message:'Possível sinal'},
                {line:'roulette',present:true,message:'Roleta: '},
                {line:'strategy',present:true,message:'Estratégia: '},
                {line:'links',present:false,message:'Links: '}
            ]},
            {category:'signal',lines:[
                {line:'title',present:true,message:'Sinal confirmado'},
                {line:'roulette',present:true,message:'Roleta: '},
                {line:'strategy',present:true,message:'Estratégia: '},
                {line:'enter',present:true,message:'Entrada: '},
                {line:'after',present:true,message:'Depois de: '},
                {line:'cover_zero',present:true,message:'Cobrir zero'},
                {line:'links',present:false,message:'Links: '}
            ]},
            {category:'result',lines:[
                {line:'green',present:true,message:'GREEN'},
                {line:'red',present:true,message:'RED'},
                {line:'result',present:true,message:'**'}
            ]},
            {category:'edit',lines:[
                {line:'title',present:true,message:'Sinal finalizado'},
                {line:'roulette',present:true,message:'Roleta: '},
                {line:'strategy',present:true,message:'Estratégia: '}
            ]},
        ]
        for(const message of messages){
            for(const line of message.lines){
                await Message.create({monitor_id:monitor._id,category:message.category,line:line.line,present:line.present,message:line.message});
            }
        }
        console.log('Processo de criação concluído');
    }
})();