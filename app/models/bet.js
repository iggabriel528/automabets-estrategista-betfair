const mongoose = require('../database');

const BetRouletteSchema = new mongoose.Schema({
    monitor_id:{type:String,require:true},
    roulette:{type:String,required:true},
    game:{type:String,required:true},
    strategy:{type:String,required:true},
    substrategy:{type:String,required:true},
    param:{type:Number,required:true},
    descript:{type:String,required:true},
    descript_enter:{type:String,required:true},
    sended_warning:{type:Boolean, required:true, default:false},
    sended_signal:{type:Boolean, required:true, default:false},
    sended_result:{type:Boolean, required:true, default:false},
    finished:{type:Boolean, required:true, default:false},
    green:{type:Boolean,required:true,default:false},
    results:{type:Array,required:true},
    attempts:{type:Number, required:true},
    trying:{type:Number,required:true,default:0},
    cover_zero:{type:Boolean,required:true,default:false},
    created_at:{type:Date,required:true,default:Date.now}
});

const BetRoulette = mongoose.model('bets',BetRouletteSchema);
module.exports=BetRoulette