const mongoose = require('../database/index');
    
const TelegramChatSchema = new mongoose.Schema({
    monitor_id:{type:String,required:true},
    name:{type: String,required: true, default:'Novo CHAT'},
    active:{type:Boolean,required:true,default:true},
    token:{type:String,required:true,default:'...'},
    chat_id:{type:String,required:true,default:'...'},
    created_at:{type: Date,default: Date.now}
});

const TelegramChat = mongoose.model('TelegramChat',TelegramChatSchema);
module.exports = TelegramChat;
