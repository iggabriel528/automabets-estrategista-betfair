const mongoose = require('../database');
const moment = require('moment');

const AdmSchema = new mongoose.Schema({
    name:{type: String,required: true},
    email:{type: String,unique: true,required: true,lowercase:true},
    password:{type: String,required:true,select:false},
    active:{type:Boolean,required:true,default:true},
    first_access:{type:Boolean,required:true,default:false},
    home_page:{type:String,required:true,default:'game/roulette/admin/dashboard'},
    tokens:[
        {
            type:{type:String,required:true},
            value:{type:String,required:true},
            validity:{type:Date,required:true,default:moment().add(10,'minutes').format('YYYY-MM-DD HH:mm:ss')}
        }
    ],
    route_rules:[
        {
            page:{type:String,required:true},
            authorized:{type:Boolean,required:true,default:false}
        }
    ],
    created_at:{type: Date,default: Date.now}
});

const Adm = mongoose.model('Adm',AdmSchema);
module.exports = Adm;
