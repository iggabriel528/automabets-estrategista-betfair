const mongoose = require('../database/index');
    
const RouletteParamSchema = new mongoose.Schema({
    monitor_id:{type:String,required:true},
    active:{type:Boolean,required:true,default:true},
    roulette:{type:String,required:true},
    strategy:{type:String,required:true},
    substrategy:{type:String,required:true},
    param:{type:Number,required:true},
    attempts:{type:Number,required:true},
    cover_zero:{type:Boolean,required:true,default:false},
    created_at:{type: Date,default: Date.now}
});

const RouletteParam = mongoose.model('RouletteParam',RouletteParamSchema);
module.exports = RouletteParam;
