const mongoose = require('../database/index');
    
const MessageSchema = new mongoose.Schema({
    monitor_id:{type:String,required:true},
    category:{type:String,required:true},
    line:{type:String,required:true},
    present:{type:Boolean,required:true,default:true},
    message:{type:String,required:true},
    created_at:{type: Date,default: Date.now}
});

const Message = mongoose.model('Message',MessageSchema);
module.exports = Message;
