const { logerror, sleep, log } = require("../tools");
const {analisys} = require("./analisys");
const { queryRoulette, addRoulette, updateRoulette } = require("./roulettes");
const moment = require('moment');

async function playtech(browser){
    const page = await browser.newPage();
    await page.setViewport({width:2000,height:2000})
    var lastrefresh = parseInt(moment().format('x'));
    loadLobby();
    async function loadLobby(){
        await page.goto('https://launcher.betfair.com/?gameId=live-roulette-cptl&channel=casino&returnURL=https%3A%2F%2Fcasino.betfair.com%2Fpt-br%2Fp%2Fcassino-ao-vivo&launchProduct=gaming&RPBucket=gaming&mode=real&dataChannel=ecasino&switchedToPopup=true');
        await page.waitForSelector('[class="lobby-tables__item"]').then(async()=>{
            var timer = setInterval(async()=>{
                var actualtimer = parseInt(moment().format('x'));
                if(actualtimer-lastrefresh > 120000){
                    clearInterval(timer);
                    log(`Playtech :: Recarregando lobby`);
                    lastrefresh = actualtimer;
                    loadLobby();
                }else{
                    const roulettes = await page.evaluate(()=>{
                        var _roulettes = [];
                        var roulettes = document.querySelectorAll('[class="lobby-tables__item"]');
                        for(var roulette of roulettes){
                            var roulette_name = roulette.querySelector('[class="lobby-table__name-container"]');
                            var roulette_results = roulette.querySelectorAll('[class*="roulette-history-item__value-text"]');
                            if(roulette_name && roulette_results){
                                var resultsarray = [];
                                for(const result of roulette_results){
                                    resultsarray.push(parseInt(result.innerText));
                                }
                                if(roulette_name.innerText!='Age Of The Gods Bonus Roulette'){
                                    _roulettes.push({
                                        roulette:roulette_name.innerText,
                                        results:resultsarray,
                                        game:'betfair_playtech'
                                    });
                                }
                            }
                        }
                        return _roulettes;
                    });
                    if(roulettes.length>0){
                        for(var roulette of roulettes){
                            if(queryRoulette(roulette)==undefined){
                                log(`Playtech: Iniciando roleta ${roulette.roulette}`);
                                addRoulette(roulette);
                                analisys(roulette);
                            }else{
                                var roulettesaved = queryRoulette(roulette);
                                var diference = false;
                                for(var i=0;i<10;i++){
                                    if(roulettesaved.results[i]!=roulette.results[i]){
                                        diference = true;
                                    }
                                }
                                if(diference){
                                    roulettesaved.results.reverse();
                                    roulettesaved.results.push(roulette.results[0]);
                                    roulettesaved.results.reverse();
                                    var valid = true;
                                    for(var i=0;i<10;i++){
                                        if(roulettesaved.results[i]!=roulette.results[i]){
                                            valid = false;
                                        }
                                    }
                                    for(var i=0;i<roulettesaved.results.length;i++){
                                        if(i>30){roulettesaved.results.splice(i,1)};
                                    }
                                    if(valid){
                                        log(`Playtech :: ${roulette.roulette} :: ${roulette.results[0]}`);
                                        updateRoulette(roulettesaved);
                                        analisys(roulette);
                                    }else{
                                        log(`Playtech :: ${roulette.roulette} :: Reiniciada por inconsistência`);
                                        console.log(roulettesaved.results.join('|'), roulette.results.join('|'))
                                        updateRoulette(roulette);
                                        analisys(roulette);
                                    }
                                }
                            }
                        }
                    }
                }
            },1000);
        })
    }
}

module.exports = playtech;