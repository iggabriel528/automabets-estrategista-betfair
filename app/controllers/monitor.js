const { Numbers } = require("../constants/numbers");
const BetRoulette = require("../models/bet");
const Chat = require("../models/chat");
const Message = require("../models/message");
const { sleep, log } = require("../tools");
const { queryRoulette } = require("./roulettes");
const axios = require('axios').default;

var openedsignals = [];

async function monitor(bet){
    if(isopened(bet)){return true};
    const chats = await Chat.find({monitor_id:bet.monitor_id, active:true});
    if(chats.length>0){
        const { monitor_id, game, roulette, results, strategy, substrategy, descript, descript_enter, need_signal, need_green, attempts, cover_zero, param, item } = bet;
        opensignal(bet,chats);
        telegram.send('warning',bet);
        var { _id } = await BetRoulette.create({monitor_id, game, roulette, strategy, substrategy, param, descript, descript_enter, sended_warning:true, finished:false, green:false, results, attempts, trying:0, cover_zero });
        const warningresultsstring = results.join('|');
        const warningresultsarray = warningresultsstring.split('|');
        var signalresultsarray = await waitResult(roulette,game,warningresultsstring);
        var signalresultsstring = signalresultsarray.join('|');

        var signalvalid = true;
        for(var i=0;i<10;i++){
            if(parseInt(signalresultsarray[i+1])!=parseInt(warningresultsarray[i])){
                signalvalid = false;
            }
        }
        if(signalvalid){
            if(need_signal.indexOf(Numbers[parseInt(signalresultsarray[0])][substrategy])>-1){
                telegram.send('signal',bet);
                telegram.send('delete',bet);
                await BetRoulette.updateOne({_id},{sended_signal:true, results:signalresultsarray});
                for(var i=1;i<=attempts;i++){
                    const signalresultsarray = await waitResult(roulette,game,signalresultsstring);
                    signalresultsstring = signalresultsarray.join('|');
                    if(need_green.indexOf(Numbers[parseInt(signalresultsarray[0])][substrategy])>-1 || (parseInt(signalresultsarray[0])==0 && cover_zero)){
                        await BetRoulette.updateOne({_id},{sended_result:true,results:signalresultsarray,green:true,finished:true,trying:i});
                        bet.results = signalresultsarray;
                        bet.green = true;
                        bet.trying = i;
                        telegram.send('edit',bet);
                        telegram.send('result',bet);
                        i = attempts;
                        finish(bet);
                    }else{
                        if(i==attempts){
                            await BetRoulette.updateOne({_id},{sended_result:true,results:signalresultsarray,green:false,finished:true,trying:i});
                            bet.results = signalresultsarray;
                            bet.green = false;
                            bet.trying = i;
                            telegram.send('edit',bet);
                            telegram.send('result',bet);
                            i = attempts;
                            finish(bet);
                        }else{
                            await BetRoulette.updateOne({_id},{results:signalresultsarray,trying:i});
                        }
                    }
                }
            }else{
                telegram.send('delete',bet);
                await BetRoulette.deleteOne({_id});
                finish(bet);
            }
        }else{
            telegram.send('delete',bet);
            await BetRoulette.deleteOne({_id});
            finish(bet);
        }
    }
}

async function waitResult(roulette, game, results){
    const lastresults = results.split('|');
    var active = true;
    while(active){
        const newresults = queryRoulette({roulette,game}).results;
        for(var i=0;i<10;i++){
            if(parseInt(lastresults[i])!=parseInt(newresults[i])){
                active = false;
                return newresults;
            }
        }
        await sleep(1000);
    }
}

function finish(params){
    const { roulette, strategy, substrategy, monitor_id } = params;
    for(var i in openedsignals){
        if(openedsignals[i].roulette==roulette && openedsignals[i].strategy==strategy && openedsignals[i].substrategy==substrategy && openedsignals[i].monitor_id==monitor_id){
            openedsignals.splice(i,1);
        }
    }
}

function isopened(params){
    const { roulette, strategy, substrategy, monitor_id } = params;
    for(var i in openedsignals){
        if(openedsignals[i].roulette==roulette && openedsignals[i].strategy==strategy && openedsignals[i].substrategy==substrategy && openedsignals[i].monitor_id==monitor_id){
            return true;
        }
    }
}

function opensignal(params,chats){
    const { roulette, strategy, substrategy, monitor_id } = params;
    openedsignals.push({roulette, strategy, substrategy, monitor_id, chats:[]});
    for(var i in openedsignals){
        if(openedsignals[i].roulette==roulette && openedsignals[i].strategy==strategy && openedsignals[i].substrategy==substrategy && openedsignals[i].monitor_id==monitor_id){
            for(var chat of chats){
                openedsignals[i].chats.push({
                    telegram_token:chat.token,
                    chat_id:chat.chat_id,
                    warning_id:0,
                    signal_id:0
                })
            }
        }
    }
}

const telegram = {
    send:async(type,params)=>{
        const { roulette, strategy, substrategy, monitor_id } = params;
        for(var i in openedsignals){
            if(openedsignals[i].roulette==roulette && openedsignals[i].strategy==strategy && openedsignals[i].substrategy==substrategy && openedsignals[i].monitor_id==monitor_id){
                const { chats } = openedsignals[i];
                for(const chat of chats){
                    telegram[type](params,chat)}}}},
    warning:async(params,chat)=>{
        const { roulette, strategy, substrategy, descript, monitor_id } = params;
        const { chat_id, telegram_token } = chat;
        var messages = await Message.find({monitor_id,category:'warning'});
        var text = '';
        if(messages.filter((e)=>{return e.line=='title'})[0].present){text+=messages.filter((e)=>{return e.line=='title'})[0].message + '\n'};
        if(messages.filter((e)=>{return e.line=='roulette'})[0].present){text+=messages.filter((e)=>{return e.line=='roulette'})[0].message + roulette + '\n'};
        if(messages.filter((e)=>{return e.line=='strategy'})[0].present){text+=messages.filter((e)=>{return e.line=='strategy'})[0].message + descript + '\n'};
        try {
            const response = await axios.get(`https://api.telegram.org/bot${telegram_token}/sendMessage`,{params:{ chat_id, text, parse_mode:'Markdown' }});
            const { ok, result } = response.data;
            if(ok){
                for(var i in openedsignals){
                    if(openedsignals[i].roulette==roulette && openedsignals[i].strategy==strategy && openedsignals[i].substrategy==substrategy && openedsignals[i].monitor_id==monitor_id){
                        for(var j in openedsignals[i].chats){
                            if(openedsignals[i].chats[j].telegram_token==telegram_token && openedsignals[i].chats[j].chat_id==chat_id){
                                openedsignals[i].chats[j].warning_id = result.message_id}}}}};
        } catch (error) {console.log(error)}
    },
    signal:async(params,chat)=>{
        const { roulette, strategy, substrategy, descript, descript_enter, results, monitor_id } = params;
        const { chat_id, telegram_token } = chat;
        var messages = await Message.find({monitor_id,category:'signal'});
        var text = '';
        if(messages.filter((e)=>{return e.line=='title'})[0].present){text+=messages.filter((e)=>{return e.line=='title'})[0].message + '\n'};
        if(messages.filter((e)=>{return e.line=='roulette'})[0].present){text+=messages.filter((e)=>{return e.line=='roulette'})[0].message + roulette + '\n'};
        if(messages.filter((e)=>{return e.line=='strategy'})[0].present){text+=messages.filter((e)=>{return e.line=='strategy'})[0].message + descript + '\n'};
        if(messages.filter((e)=>{return e.line=='enter'})[0].present){text+=messages.filter((e)=>{return e.line=='enter'})[0].message + descript_enter + '\n'};
        if(messages.filter((e)=>{return e.line=='after'})[0].present){text+=messages.filter((e)=>{return e.line=='after'})[0].message + telegram.resultstostring(results, 3) + '\n'};
        if(messages.filter((e)=>{return e.line=='cover_zero'})[0].present){text+=messages.filter((e)=>{return e.line=='cover_zero'})[0].message + '\n'};
        try {
            const response = await axios.get(`https://api.telegram.org/bot${telegram_token}/sendMessage`,{params:{ chat_id, text, parse_mode:'Markdown' }});
            const { ok, result } = response.data;
            if(ok){
                for(var i in openedsignals){
                    if(openedsignals[i].roulette==roulette && openedsignals[i].strategy==strategy && openedsignals[i].substrategy==substrategy && openedsignals[i].monitor_id==monitor_id){
                        for(var j in openedsignals[i].chats){
                            if(openedsignals[i].chats[j].telegram_token==telegram_token && openedsignals[i].chats[j].chat_id==chat_id){
                                openedsignals[i].chats[j].signal_id = result.message_id}}}}};
        } catch (error) {console.log(error)}
    },
    result:async(params,chat)=>{
        const { green, results, trying, monitor_id } = params;
        const { telegram_token, chat_id, signal_id } = chat;
        var messages = await Message.find({monitor_id,category:'result'});
        var text = '';
        if(green && messages.filter((e)=>{return e.line=='green'})[0].present){text+=messages.filter((e)=>{return e.line=='green'})[0].message + '\n'};
        if(!green && messages.filter((e)=>{return e.line=='red'})[0].present){text+=messages.filter((e)=>{return e.line=='red'})[0].message + '\n'};
        if(messages.filter((e)=>{return e.line=='result'})[0].present){text+=messages.filter((e)=>{return e.line=='result'})[0].message + telegram.resultstostring(results,trying) + '\n'};
        try {await axios.get(`https://api.telegram.org/bot${telegram_token}/sendMessage`,{params:{ chat_id, text, parse_mode:'Markdown', reply_to_message_id:signal_id }})}catch(error){console.log(error)}
    },
    delete:async(params,chat)=>{
        const { chat_id, telegram_token, warning_id } = chat;
        try{await axios.get(`https://api.telegram.org/bot${telegram_token}/deleteMessage`,{params:{ chat_id, message_id:warning_id }})}catch(error){console.log(error)}
    },
    edit:async(params,chat)=>{
        const { monitor_id } = params;
        const { chat_id, telegram_token, signal_id } = chat;
        var messages = await Message.find({monitor_id,category:'edit'});
        var text = '';
        if(messages.filter((e)=>{return e.line=='title'})[0].present){text+=messages.filter((e)=>{return e.line=='title'})[0].message + '\n'};
        if(messages.filter((e)=>{return e.line=='roulette'})[0].present){text+=messages.filter((e)=>{return e.line=='roulette'})[0].message + params.roulette + '\n'};
        if(messages.filter((e)=>{return e.line=='strategy'})[0].present){text+=messages.filter((e)=>{return e.line=='strategy'})[0].message + params.descript + '\n'};
        if(text!=''){
            try{await axios.get(`https://api.telegram.org/bot${telegram_token}/editMessageText`,{params:{ chat_id, text, parse_mode:'Markdown', message_id:signal_id }});}catch(error){console.log(error)}
        }
    },
    resultstostring:(results, index)=>{
        var newresults = [];
        for(var i=0;i<index;i++){
            newresults.push(results[i]);
        }
        newresults.reverse();
        return newresults.join(' | ')
    }
}

module.exports = telegram;


module.exports = monitor