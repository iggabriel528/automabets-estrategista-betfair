const GameHistory = require('../models/gamehistory');
const Monitor = require('../models/monitor');
const Param = require('../models/rouletteparam');
const strategies = require('./strategies');

async function analisys(data){
    const { roulette, game, results } = data;
    const monitores = await Monitor.find({game,active:true,active_adm:true});
    for(const monitor of monitores){
        const params = await Param.find({monitor_id:monitor._id, roulette, active:true});
        for(var param of params){
            strategies[param.strategy](data,param);
        }
    }
    await GameHistory.create({game, name:roulette, result:results[0]});
}

module.exports = {analisys};