const RouletteParam = require('./app/models/rouletteparam');

const actual = require('./params.json');

(async()=>{
    const monitor_id = '6222503068d9829326ba2cb6';
    for(var act of actual){
        const { param, cover_zero, active, roulette, attempts, strategy, substrategy } = act;
        console.log(await RouletteParam.updateOne({roulette,monitor_id,strategy,substrategy},{param,cover_zero,active,attempts}));
    }
})();
